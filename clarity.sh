#!/bin/bash

# https://github.com/qzb/is.sh/blob/master/is.sh
is_file() {
    [ -f "$1" ]; return $?
}

is_directory() {
    [ ! -d "$1" ]; return $?
}

is_dir() {
    [ -d "$1" ]; return $?
}

is_symlink() {
    [ -L "$1" ]; return $?
}

exists() {
    [ -e "$1" ]; return $?
}

is_readable() {
    [ -r "$1" ]; return $?
}

is_writeable() {
    [ -w "$1" ]; return $?
}

is_executable() {
    [ -x "$1" ]; return $?
}

is_available(){
    which "$1"; return $?
}

is_newer() {
    [ "$1" -nt "$1" ]; return $?
}

is_older(){
    [ "$1" -ot "$1" ]; return $?
}

is_number() {
    echo "$1" | grep -q -E '^-?[0-9]+(\.[0-9]+)?$'; return $?
}

is_integer() {
    echo "$1" | grep -q -E '^-?[0-9]*$'; return $?
}

contains() {
    echo "$1" | grep -q -F "$2"; return $?
}

is_empty() {
    [ -z "$1" ]; return $?
}

is_true() {
    [ "$1" == true ] || [ "$1" == 0 ]; return $?
}

is_false() {
    [ "$1" != true ] && [ "$1" != 0 ]; return $?
}
